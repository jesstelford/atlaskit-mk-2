// @flow

module.exports = {
  'packages/core/navigation-next/src': {
    statements: 68,
    branches: 60,
    functions: 68,
    lines: 68,
  },
  'packages/core/global-navigation/src': {
    statements: 85,
    branches: 85,
    functions: 85,
    lines: 85,
  },
};
